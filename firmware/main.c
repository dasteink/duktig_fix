/* Name: main.c
 * Author: Daniel Steinke
 * Copyright: 2013, all rights reserverd
 * License: GPLv3
 */
#define F_CPU 1000000UL
#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>

#define __setbit(reg, bit) reg|=_BV(bit)
#define __clearbit(reg, bit) reg&=~_BV(bit)

#define Key1Pin PB0
#define Key2Pin PB1

#define LED_OFF 0
#define LED_ON 1



//
// Debounce macro from Peter Dannegger 
//
#define debounce( port, pin )                                             \
({                                                                        \
    static uint8_t flag = 0;     /* new variable on every macro usage */  \
    uint8_t i = 0;                                                        \
                                                                          \
    if( flag ){                  /* check for key release: */             \
        for(;;){                   /* loop ... */                         \
            if( !(port & 1<<pin) ){  /* ... until key pressed or ... */   \
                i = 0;                 /* 0 = bounce */                   \
                break;                                                    \
            }                                                             \
            _delay_us( 98 );         /* * 256 = 25ms */                   \
            if( --i == 0 ){          /* ... until key >25ms released */   \
                flag = 0;              /* clear press flag */             \
                i = 0;                 /* 0 = key release debounced */    \
                break;                                                    \
            }                                                             \
        }                                                                 \
    }else{                       /* else check for key press: */          \
        for(;;){                   /* loop ... */                         \
            if( (port & 1<<pin) ){   /* ... until key released or ... */  \
                i = 0;                 /* 0 = bounce */                   \
                break;                                                    \
            }                                                             \
            _delay_us( 98 );         /* * 256 = 25ms */                   \
            if( --i == 0 ){          /* ... until key >25ms pressed */    \
                flag = 1;              /* set press flag */               \
                i = 1;                 /* 1 = key press debounced */      \
                break;                                                    \
            }                                                             \
        }                                                                 \
    }                                                                     \
    i;                           /* return value of Macro */              \
})

//
// Global variables for useage in ISR signaling
//
uint8_t volatile keypressed0 = 0;
uint8_t volatile keypressed1 = 0;
uint8_t volatile timercounter0 = 0;
uint8_t volatile timercounter1 = 0;

//
// Interrupt service routines
// Content of ISRs kept minial - only signaling event to main loop
//
ISR(PCINT1_vect){
    keypressed1 = 1;
    return;
}

ISR(PCINT0_vect){
    keypressed0 = 1;
    return;
}

ISR(TIM0_OVF_vect){
    timercounter0++;
    timercounter1++;
}

int main(void)
{
    
    // Configure PORTA
    DDRA = 0x00;          // Init DDRA
    __setbit(DDRA, PA5);  // PA5 and PA6 of PORTA as output
    __setbit(DDRA, PA6);  // PA5 = LED 1 (OC1A)
                          // PA6 = LED 2 (OC1B)
    __setbit(DDRA, PA0);  // Debug Output (for use with scope)
    PORTA = 0x9E;         // = 0b1001 1110 Enable Pull-ups on all ports excep PA0, PA5 and PA6
    
    // Configure PORTB
    DDRB = 0x00;        // All PortB Pins are Inputs
    PORTB = 0xFF;       // Enable internal Pull-ups
   
    // Configure Pin change interrupts
    PCMSK0 = 0;                 // Disable all Pin Change interrupts for PCIE0
    __setbit(PCMSK0, PCINT1);   // Enable Pin Change interrupt on PA1
    PCMSK1 = 0;                 // Disable all Pin Change interrupts for PCIE1
    __setbit(PCMSK1, PCINT8);   // Enable Pin Change interrupt on PB0

    __setbit(GIMSK, PCIE0);     // enable PCINT0 interrupt group
    __setbit(GIMSK, PCIE1);     // enable PCINT1 interrupt group
    
    // Configure initial sleep mode.
    set_sleep_mode(SLEEP_MODE_IDLE);
    
    // Configure Timer1 for PWM generation
    TCCR1A = (1<<WGM10);            // PWM, phase correct, 8 bit.
    __setbit(TCCR1B, CS10);
    //TCCR1B = (1<<CS11) | (1<<CS10); // Prescaler 64 = Enable counter
    OCR1A = 85;                     // Duty cycle for PWM channel A
    OCR1B = 85;                     // Duty cycle for PWM channel B
    
    // Configure Timer0 for LED timeout generation
    TCCR0A = 0;                     // No PWM used on Timer0
    __setbit(TCCR0B, CS02);
    __setbit(TCCR0B, CS00);         // Prescaler 1024
    __setbit(TIMSK0, TOIE0);        // Enable overflow interrupt
    
    // Disable unused hardware
    __setbit(PRR, PRUSI);
    __setbit(ACSR, ACD);
    __setbit(PRR, PRADC);
    
    sei();
    
    //
    uint8_t LED1_state = LED_OFF;
    uint8_t LED2_state = LED_OFF;
    
    __clearbit(PORTA, PA0);
    
    for(;;){
        
        //
        // Handle state changes caused by pressing a key
        //
        if (keypressed0) // Keypress interrupt has been triggered
                        // Find out, which key has been pressed
        {            
            if (debounce(PINA,PA1))
            {
                // Key 1 has been pressed
                // Do state transitions
                switch (LED1_state)
                {
                    case LED_OFF: {
                        LED1_state = LED_ON;
                        timercounter0 = 0;
                        break;
                    }
                    case LED_ON: {
                        LED1_state = LED_OFF;
                        break;
                    }
                    default:
                        break;
                }
            }
            keypressed0 = 0;            
        }
        
        if (keypressed1)
        {
            if (debounce(PINB, PB0))
            {
                // Key 2 has been pressed
                // Do state transitions
                switch (LED2_state) {
                    case LED_OFF: {
                        LED2_state = LED_ON;
                        timercounter1 = 0;
                        break;
                    }
                    case LED_ON: {
                        LED2_state = LED_OFF;
                        break;
                    }
                    default:
                        break;
                }
            }
            keypressed1 = 0;
        }
        
        //
        // Handle state changes caused by timeouts
        //
        if ((timercounter0>=255)&(LED1_state==LED_ON)) {
            LED1_state = LED_OFF;
        }
        if ((timercounter1>=255)&(LED2_state==LED_ON)) {
            LED2_state = LED_OFF;
        }
        
        //
        // Enable/Disable LEDs based in state
        // Makes use of PWM channel enabling/disabling in Timer1 module
        //
        if (LED1_state == LED_ON) __setbit(TCCR1A, COM1A1);
        else __clearbit(TCCR1A, COM1A1);
        
        if (LED2_state == LED_ON) __setbit(TCCR1A, COM1B1);
        else __clearbit(TCCR1A, COM1B1);
        
        
        if ((LED1_state == LED_ON)|(LED2_state == LED_ON))
        {
            sleep_disable();
            set_sleep_mode(SLEEP_MODE_IDLE); // If one of the LEDs is on -> go to idle to keep PWM running
            sleep_enable();
            
            __setbit(PORTA, PA0);
            _delay_us(20);
            __clearbit(PORTA, PA0);
        }
        else
        {
            sleep_disable();
            set_sleep_mode(SLEEP_MODE_PWR_DOWN); // If no LED is on -> power down mode to save max. power
            sleep_enable();
            
            __setbit(PORTA, PA0);
            _delay_us(40);
            __clearbit(PORTA, PA0);
        }
        
        sleep_cpu();     // CPU wakes up on keypress
        
    }
    return 0;   /* never reached */
}
